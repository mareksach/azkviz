const SHAPES = {
    circle: `<svg width="100" height="100"><circle r="50" cx="50" cy="50"></svg>`,
    triangle: `<svg width="120" height="100"><polygon points="60,0 0,100 120,100" /></svg>`,
    square: `<svg width="100" height="100"><rect width="100" height="100"/></svg>`,
    hexagon: `<svg width="100" height="80"><polygon points="25,0 0,40 25,80 75,80 100,40 75,0" /></svg>`
};

const COLORS = {
    orange: "#ff6f00",
    lightblue: "#29ceff",
};

const GAME_STATE = {
    IDLE: 0,
    RUNNING: 1
};

const SHAPE_CHANGE_PERIOD = 500;

function getRandomDictItem(dict) {
    const keys = Object.keys(dict);
    const idx = Math.floor(Math.random() * keys.length);
    return dict[keys[idx]];
}

function setRandomShapePair() {
    const shape1 = getRandomDictItem(SHAPES);
    const shape2 = getRandomDictItem(SHAPES);
    const color1 = getRandomDictItem(COLORS);
    const color2 = getRandomDictItem(COLORS);

    const div1 = document.querySelector("#shape1");
    const div2 = document.querySelector("#shape2");

    div1.innerHTML = shape1;
    div2.innerHTML = shape2;
    div1.style.fill = color1;
    div2.style.fill = color2;
}

function shapesAreEqual() {
    const div1 = document.querySelector("#shape1");
    const div2 = document.querySelector("#shape2");
    return div1.innerHTML != ""
        && div1.innerHTML == div2.innerHTML
        && div1.style.fill == div2.style.fill;
}

function evaluateDraw(clickedButton) {
    if (currGameState == GAME_STATE.RUNNING) {
        clearInterval(shapeGenerator);
        if (shapesAreEqual()) {
            clickedButton.classList.add("winner")
        } else {
            clickedButton.classList.add("looser")
        }
        currGameState = GAME_STATE.IDLE;
    }
}

function startNewDraw() {
    if (currGameState == GAME_STATE.IDLE) {
        const div1 = document.querySelector("#shape1");
        const div2 = document.querySelector("#shape2");
        div1.innerHTML = "";
        div2.innerHTML = "";

        shapeGenerator = setInterval(setRandomShapePair, SHAPE_CHANGE_PERIOD);
        currGameState = GAME_STATE.RUNNING;
        for (let i = 0; i < playerButtons.length; i++) {
            playerButtons[i].classList.remove("winner");
            playerButtons[i].classList.remove("looser");
        }
    }
}

// GAME STATE VARIABLES
let currGameState = GAME_STATE.IDLE;
let shapeGenerator = null;

// GAME LOGIC
const startGameBtn = document.querySelector("#start-game-btn");
startGameBtn.addEventListener("click", startNewDraw);

const playerButtons = document.querySelectorAll("#draw-container button");
for (let i = 0; i < playerButtons.length; i++) {
    playerButtons[i].addEventListener("click", () => {
        evaluateDraw(playerButtons[i]);
    });

    playerButtons[i].addEventListener("touchstart", () => {
        evaluateDraw(playerButtons[i]);
    });
}

document.addEventListener("keydown", (event) => {
    switch (event.key.toLowerCase()) {
        case "n":
            startNewDraw();
            break;
        case "a":
            evaluateDraw(document.querySelector("#player1-btn"));
            break;
        case "l":
            evaluateDraw(document.querySelector("#player2-btn"));
            break;
    }
});