![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

# AZ kvíz - draw equipment

This is a single page implementation of a draw equipment from a well known czech TV competition *AZ kvíz*.

Basically, this app is a very simple two player game.
After the game is started, a pair of shapes appears. The shapes and their color are changing over time.

The goal of this game is to click a button (each player has his own button),
as soon as two identical shapes (also with the same color) appear.
The first player, who clicks correctly (when two identical shapes are shown), wins.
If the player clicks incorrectly (the currently shown shapes differ), he looses.

The app is designed for use on desktop (controlled via keyboard), as well as for mobile devices.
